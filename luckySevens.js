function playGame(){
	var die1 = document.getElementById("die1");
	var die2 = document.getElementById("die2");
	var status = document.getElementById("status");
	var Bet = (document.getElementById("BetField").value);
	var BetField = Number(Bet);
	var diceRolls = 0;
	var rollsMost = 1;
	var mostMoney = BetField;
	var profit = 4 ;
	var loss = 1 ;
		if (Bet <= 0){
			return alert("Your Bet Must be Higher than 0.");
		}
		if (isNaN(Bet)){
			return alert("Your Bet needs to be a Number.");
		}
		Bet = Number(Bet)
		while (Bet > 0) {
		var d1 = Math.floor(Math.random()*6)+1;
		var d2 = Math.floor(Math.random()*6)+1;
		var diceTotal = d1+d2;
		totalDiceRolls = diceRolls ++;
		if (diceTotal ==7){
		  Bet += profit;
		  totalMostMoney=Bet;
		}
		else {
			Bet -= loss;
		}
		if (Bet>mostMoney){
			totalRollsMost = diceRolls;
			}
		  else if(Bet<=0){ 
			results();
			playAgain();
		  }
	  }
}
	  
function results(){

	document.getElementById("BetResult").innerHTML ="$" +(Number(BetField)).toFixed(2);	
	document.getElementById("diceRollsResult").innerHTML = totalDiceRolls;
	document.getElementById("mostMoneyResult").innerHTML = "$" + (Number(totalMostMoney)-1).toFixed(2);
	document.getElementById("rollsMostResult").innerHTML = totalRollsMost;	
}

function playAgain() {

	document.getElementById("Button").innerHTML = "Play Again?";
	document.getElementById("results").style.display = "";
}

		  